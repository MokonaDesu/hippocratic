var hippocratic = (function(hippocratic){
    hippocratic.security = hippocratic.security || {};
    
    /**
    * Indicates whether current user is internal users. Filled in by async request.
    * @property userIsInternal
    * @type {Boolean} - undefined means query is not completed yet.
    */
    hippocratic.security.userIsInternal;
    
    /**
    * Contains current user reference
    * @property currentUser
    * @type {Object}
    */
    hippocratic.security.currentUser;
    
    /**
    * Indicates whether the form has issued the query to check for user permissions.
    * @property userGroupUpdateCalled
    * @type {Boolean}
    */
    hippocratic.security.userGroupUpdateCalled = false;
    
    /**
    * Runs the query to find out if current
    * user is a member of internal user group;
    *
    * @method getIsUserInternal
    */
    function getIsUserInternal(callback) {  
        hippocratic.utils.isCurrentUserInGroup(hippocratic.utils.internalUsersGroupName, function(isMember, currentUser) {
            hippocratic.security.userIsInternal = isMember;
            hippocratic.security.currentUser = currentUser;
            callback(currentUser);
        });
    }
    
    /**
    * Performs the security checks and updates 
    * the input form controls according to 
    * current user's user group.
    *
    * @method requestUserPermissionsAndUpdateForm
    */
    hippocratic.security.updateFormForUserRole = function(ctx, callback) {
        if (!hippocratic.security.userGroupUpdateCalled) {
            hippocratic.security.userGroupUpdateCalled = true;
            getIsUserInternal(function(currentUser) {
                if (hippocratic.security.userIsInternal) {
                    hippocratic.utils.enableProtectedFields();
                } else {
                    hippocratic.utils.disableProtectedFields();
                }
                hippocratic.utils.disableReadOnlyFields();
                hippocratic.currentRenderer.addCustomButtons(hippocratic.security.userIsInternal);
                hippocratic.utils.showContent(hippocratic.security.userIsInternal);
            });
        }
    }
    
    return hippocratic;
}(hippocratic || {}));