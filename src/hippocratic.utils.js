var hippocratic = (function(hippocratic){   
    var defaultTemplates = SPClientTemplates._defaultTemplates.Fields.default.all.all;

    hippocratic.utils = hippocratic.utils || {};
    
    /**
    * Internal users groups name.
    * @property internalUsersGroupName
    * @type {String}
    */
    hippocratic.utils.internalUsersGroupName = 'Internal Users';
    
    /**
    * Indicates whether the form is disabled for the loading to complete.
    * @property formDisabled
    * @type {Boolean}
    */
    hippocratic.utils.formDisabled = false;
    
    hippocratic.utils.getParameterByName = function(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    };

    hippocratic.utils.addCancelButton = function() {
        hippocratic.utils.pushNewButton('<input type="button" name="cancel" value="Cancel" onclick="hippocratic.forms.cancelHandler(\'' + getCancelRedirectUrl() + '\')">'); 
    }
    
    hippocratic.utils.addStartProcessingButton = function() {
        hippocratic.utils.pushNewButton('<input type="button" value="Start Processing" onclick="hippocratic.forms.startProcessingHandler()">');
    }
    
    hippocratic.utils.addCompleteButton = function() {
        hippocratic.utils.pushNewButton('<input type="button" value="Complete" onclick="hippocratic.forms.completeHandler()">');   
    }
    
    hippocratic.utils.addSubmitButtons = function() {
	    hippocratic.utils.pushNewButton('<input type="button" value="Submit and Create New" onclick="hippocratic.forms.submitAndNewHandler()">'); 
	    hippocratic.utils.pushNewButton('<input type="button" value="Submit" onclick="hippocratic.forms.submitHandler()">');
    }
    
    hippocratic.utils.createUpperToolbar = function() {
        $('table.ms-formtable').before('<table id="topToolbar" width="100%" class="ms-formtoolbar" cellpadding="0" cellspacing="0"><tr class="custom-buttons-container"><td></td></tr></table>');
    }
    
    hippocratic.utils.addEditButton = function() {
	    hippocratic.utils.pushNewButton('<input type="button" value="Edit" onclick="hippocratic.forms.editHandler()">');
    }

    hippocratic.utils.pushNewButton = function (outerHtml) {
        $('table.ms-formtoolbar')
            .last()
            .find('.ms-separator')
            .first()
            .after('<td class="ms-toolbar">' + outerHtml + '</td><td class="ms-separator">&nbsp;</td>');
            
        $('table#topToolbar > tbody > tr > td:first-child').after('<td class="ms-toolbar">' + outerHtml + '</td><td class="ms-separator">&nbsp;</td>');;
    };

    hippocratic.utils.removeExistingButtons = function () {
        $('table.ms-formtoolbar')
            .last()
            .find('tr')
            .addClass('custom-buttons-container')
            .empty()
            .append('<td class="ms-separator"></td>');
        
        //Disable the ribbon save and cancel buttons.
        var commitRibbonSection = document.getElementById('Ribbon.ListForm.Edit.Commit');
        if (commitRibbonSection) commitRibbonSection.style.display = 'none';
        
        //Disable the ribbon edit button.
        var editRibbonButton = document.getElementById('Ribbon.ListForm.Display.Manage.EditItem-Large');
        if (editRibbonButton) editRibbonButton.style.display = 'none';
    };
    
    hippocratic.utils.isCurrentUserInGroup = function (groupName, onComplete) {
        var currentContext = new SP.ClientContext.get_current();
        var currentWeb = currentContext.get_web();
        var currentUser = currentContext.get_web().get_currentUser();
        currentContext.load(currentUser);
        var allGroups = currentWeb.get_siteGroups();
        currentContext.load(allGroups);
        var group = allGroups.getByName(groupName);
        currentContext.load(group);
        var groupUsers = group.get_users();
        currentContext.load(groupUsers);

        currentContext.executeQueryAsync(onSuccess, onFailure);

        function onSuccess(sender, args) {
            var userInGroup = false;
            var groupUserEnumerator = groupUsers.getEnumerator();
            while (groupUserEnumerator.moveNext()) {
                var groupUser = groupUserEnumerator.get_current();
                if (groupUser.get_id() == currentUser.get_id()) {
                    userInGroup = true;
                    break;
                }
            }
            onComplete(userInGroup, currentUser);
        }

        function onFailure(sender, args) {
            onComplete(false);
        }
    };

    hippocratic.utils.disableFields = function(wrapperDiv) {
        $(wrapperDiv).find('input.ms-input').attr('readonly', true);
        $(wrapperDiv).find('input.ms-long').attr('readonly', true);
        $(wrapperDiv).find('td.ms-dtinput>a').attr('onclick', '');
        // Leaving the checked one enabled so that form is still able to submit.
        $(wrapperDiv).find(':radio:not(:checked)').attr('disabled', true);
        $(wrapperDiv).find('div[role="textbox"]').attr('contenteditable', false);
    };
    
    hippocratic.utils.makeReadOnly = function(fieldName) {
        return function(ctx){
            var fieldType = ctx.CurrentFieldSchema.FieldType;
            return '<div class="read-only-content content-disabled">' + defaultTemplates[fieldType][ctx.BaseViewID](ctx) + '</div>';
        };
    };
    
    hippocratic.utils.makeProtected = function(fieldName){
        return function(ctx){
            var fieldType = ctx.CurrentFieldSchema.FieldType;
            return '<div class="protected-content content-disabled">' + defaultTemplates[fieldType][ctx.BaseViewID](ctx) + '</div>';
        };
    }

    hippocratic.utils.makeHidden = function(fieldName){
        return function(ctx){
            var fieldType = ctx.CurrentFieldSchema.FieldType;
            return '<div class="hiddenContainer">' + defaultTemplates[fieldType][ctx.BaseViewID](ctx) + '</div>';
        };
    }

    function getCancelRedirectUrl(userIsInternal) {
        if (userIsInternal) {
            return '/sites/home';
        } else {
            return (new SP.ClientContext()).get_url();
        }
    }

    hippocratic.utils.showContent = function (userIsInternal) {
        $('#contentBox').fadeIn();
    };
    
    hippocratic.utils.hideContent = function () {
        $('#contentBox').hide();
    };

    hippocratic.utils.buildOverridenFieldsHash = function(formFieldsInfo) {
        var fieldsInfo = {};

        function overrideAsReadOnlyField(field) {
            fieldsInfo[field] = {
                NewForm: hippocratic.utils.makeReadOnly(field),
                EditForm: hippocratic.utils.makeReadOnly(field)
            };
        }

        function overrideAsProtectedField(field) {
            fieldsInfo[field] = {
                NewForm: hippocratic.utils.makeProtected(field),
                EditForm: hippocratic.utils.makeProtected(field)
            };
        }

        function overrideAsHidden(field) {
            fieldsInfo[field] = {
                NewForm: hippocratic.utils.makeHidden(field),
                EditForm: hippocratic.utils.makeHidden(field)
            };
        }

        formFieldsInfo.readOnlyFields.forEach(overrideAsReadOnlyField);
        formFieldsInfo.protectedFields.forEach(overrideAsProtectedField);
        formFieldsInfo.hiddenFields.forEach(overrideAsHidden);
        return fieldsInfo;
    };
    
    hippocratic.utils.updateCreatedBy = function(ctx) {
        if (ctx.ListSchema.Field[0].Name == 'CreatedBy') {
            var fieldId = ctx.ListSchema.Field[0].Id;
            var context = new SP.ClientContext.get_current();
            var currentUser = context.get_web().get_currentUser();
            context.load(currentUser);
            context.executeQueryAsync(function() {
                document.getElementById('CreatedBy_' + fieldId + '_$TextField').value = currentUser.get_title();
            });
        }
    };

    hippocratic.utils.separateGroups = function (ctx, fieldsInfo) {
        ctx.ListSchema.Field.forEach(function(field) {
            if (fieldsInfo.lastInGroup.indexOf(field.Name) != -1) {
                var span = $get(ctx.FormUniqueId + ctx.FormContext.listAttributes.Id + field.Name);
                var tr = document.createElement('tr');
                tr.style.height = '50px';
                tr.innerHTML='<td colspan="2">&nbsp;</td>';
                var fieldTr = span.parentNode.parentNode;
                fieldTr.parentNode.insertBefore(tr, fieldTr.nextSibling);
            }
        });
    };
    
    hippocratic.utils.disableProtectedFields = function() {
        hippocratic.utils.disableFields($('.protected-content'));
    };
    
    hippocratic.utils.disableReadOnlyFields = function () {
        hippocratic.utils.disableFields($('.read-only-content'));
    };
    
    hippocratic.utils.enableProtectedFields = function(){
        var protectedField = $('.protected-content').each(function(index, field) {
            $(field).removeClass('content-disabled');
        });
    };
    
    hippocratic.utils.updateProcessingStatus = function(status){
        var clientContext = new SP.ClientContext();
        var oList = clientContext.get_web().get_lists().getById(hippocratic.authorizationFormRenderer.formContext.ListAttributes.Id);
        var oListItem = oList.getItemById(hippocratic.authorizationFormRenderer.itemId);
        oListItem.set_item('ProcessingStatus', status);
        oListItem.update();
        function onSuccess(e){
            window.location.href = window.location.href; 
        }
        
        function onFail(e){
            alert("Fail!!!");
        }
        clientContext.executeQueryAsync(Function.createDelegate(this, onSuccess), Function.createDelegate(this, onFail));
    };
    
    hippocratic.utils.getListName = function() {
        return location.href.match(new RegExp('/Lists/(.+)/'))[1];
    }

    return hippocratic;
}(hippocratic || {}));