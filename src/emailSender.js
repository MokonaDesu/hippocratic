var EmailSender = (function() {
    var templateFromAddress = 'noreply@hippocraticsolutions.com';
  
    var cachedEmails = { };
  
    function getUserGroupEmails(groupName, callback) {
        if (cachedEmails[groupName]) {
            if (callback) { callback(cachedEmails[groupName]) };
            return;
        }
        
        var currentContext = new SP.ClientContext.get_current();
        var currentWeb = currentContext.get_web();
        var currentUser = currentContext.get_web().get_currentUser();
        currentContext.load(currentUser);
        var allGroups = currentWeb.get_siteGroups();
        currentContext.load(allGroups);
        var group = allGroups.getByName(groupName);
        currentContext.load(group);
        var groupUsers = group.get_users();
        currentContext.load(groupUsers);
        currentContext.executeQueryAsync(onSuccess, onFailure);

        function onSuccess(sender, args) {
            var userInGroup = false;
            var groupUserEnumerator = groupUsers.getEnumerator();
            var emails = [];
            while (groupUserEnumerator.moveNext()) {
                var groupUser = groupUserEnumerator.get_current();
                emails.push(groupUser.get_email());
            }
        
            cachedEmails[groupName] = emails;
            if (callback) { callback(emails); }
        }

        function onFailure(sender, args) {
            if (callback) { callback([]); }
        }
    }
    
    return {
        sendEmail: function(from, to, body, subject, callback) {
            $.ajax({
                contentType: 'application/json',  
                url: _spPageContextInfo.webServerRelativeUrl + "/_api/SP.Utilities.Utility.SendEmail",
                type: "POST",
        
                data: JSON.stringify({
                    'properties': {
                        '__metadata': { 'type': 'SP.Utilities.EmailProperties' },
                        'From': from,
                        'To': { 'results': to },
                        'Body': body,
                        'Subject': subject
                    }
                }),
        
                headers: {
                    "Accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                },
        
                success: function(data) {
                    if (callback) callback(true, data);
                },
        
                error: function(data) {
                    if (callback) callback(false, data);
                }
            });
        },
    
        sendEmailFromTemplate: function(template, callback) {
            this.sendEmail(template.from, template.to, template.body, template.subject, callback);
        },
    
        newAuthorizationFormTemplate: function(to, meta) {
            return {
                from: templateFromAddress,
                subject: 'New authorization form created!',
                to: to,
                body: '<p>Hello,<br><br>New authorization form was submitted by ' + meta.practice + ' for your approval. Click <a href="' + meta.link + '">here</a> to navigate to the list.</p>'
            };
        },
    
        newPatientPaymentFormTemplate: function(to, meta) {
            return {
                from: templateFromAddress,
                subject: 'New patient payment!',
                to: to,
                body: '<p>Hello,<br><br>New patient payment form was submitted by ' + meta.practice + '. Click <a href="' + meta.link + '">here</a> to navigate to the list.</p>'
            };
        },
    
        newCopayPaymentFormTemplate: function(to, meta) {
            return {
                from: templateFromAddress,
                subject: 'New Copay payment!',
                to: to,
                body: '<p>Hello,<br><br>New Copay payment form was submitted by ' + meta.practice + '. Click <a href="' + meta.link + '">here</a> to navigate to the list.</p>'
            };
        },
    
        getUserGroupEmails: getUserGroupEmails
    };
})();