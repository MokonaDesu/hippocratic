/* global SPClientTemplates */
/**
* AuthorizationFormRenderer
*/
var hippocratic = (function (hippocratic) {
    hippocratic.authorizationFormRenderer = hippocratic.authorizationFormRenderer || {};
    hippocratic.currentRenderer = hippocratic.authorizationFormRenderer;
    hippocratic.authorizationFormRenderer.processingStatus = "Invalid";
    hippocratic.authorizationFormRenderer.formContext = null;
    hippocratic.authorizationFormRenderer.itemId = -1;
    hippocratic.authorizationFormRenderer.notificationTargetGroup = 'Authorization Form Watchers';
    hippocratic.authorizationFormRenderer.notificationTemplate = 'newAuthorizationFormTemplate';
    hippocratic.authorizationFormRenderer.practice;
    var prefixesRemoved = false;
    
    hippocratic.authorizationFormRenderer.processingStatuses = {
       Created: "Created",
       Processing: "InProcessing",
       Completed: "Completed"
    };
        
    hippocratic.authorizationFormRenderer.addCustomButtons = function(userIsInternal) {
        hippocratic.utils.removeExistingButtons();
        hippocratic.utils.createUpperToolbar();
        hippocratic.utils.addCancelButton();
        
        if (userIsInternal) {
            if (hippocratic.authorizationFormRenderer.processingStatus == hippocratic.authorizationFormRenderer.processingStatuses.Created && 
				hippocratic.authorizationFormRenderer.formContext.FormControlMode != 3) {
                hippocratic.utils.addStartProcessingButton();
            }
            if (hippocratic.authorizationFormRenderer.processingStatus == hippocratic.authorizationFormRenderer.processingStatuses.Processing) {
				hippocratic.utils.addCompleteButton();
            }    
        } else {
            if (hippocratic.authorizationFormRenderer.processingStatus == hippocratic.authorizationFormRenderer.processingStatuses.Created) {
                hippocratic.utils.addSubmitButtons();
            }
        }
    }

    /**
    * Changes out labels on certain fields.
    *
    * @method postRenderLocationLabel
    * @param {Object} CSR context.
    */
    function renderFieldLabels(ctx) {
        switch(ctx.ListSchema.Field[0].Name) {
            case 'HippocraticLocation': 
                var containingTr = $('table#HippocraticLocation_' + ctx.ListSchema.Field[0].Id + '_MultiChoiceTable').closest('tr');
                var nobt = containingTr.find('nobr').text(containingTr.find('nobr').text().replace('HippocraticLocation', 'Location'));
                break;     
            case 'CreatedBy': 
                var containingTr = $(document.getElementById('CreatedBy_' + ctx.ListSchema.Field[0].Id + '_$TextField')).closest('tr');
                var nobt = containingTr.find('nobr').text(containingTr.find('nobr').text().replace('Hippocratic Created By', 'Created By'));
                break;
            case 'StartDate1':
                var containingTr = $(document.getElementById('StartDate1_' + ctx.ListSchema.Field[0].Id + '_$DateTimeFieldDate')).closest('div.protected-content').closest('tr');
                var nobt = containingTr.find('nobr').text(containingTr.find('nobr').text().replace('Hippocratic Start Date', 'Start Date'));
                break;
            case 'EndDate1': 
                var containingTr = $(document.getElementById('EndDate1_' + ctx.ListSchema.Field[0].Id + '_$DateTimeFieldDate')).closest('div.protected-content').closest('tr');
                var nobt = containingTr.find('nobr').text(containingTr.find('nobr').text().replace('Hippocratic End Date', 'End Date'));
                break;
            case 'ProcessingStatus':
				$("input[title='ProcessingStatus']").closest('tr').addClass("hiddenContainer");
                break;
            default:
                removePrefixes(ctx);
        }
    }
    
    function getFieldProperName(field) {
        var newLabel = field;
        hippocratic.fields.authorizationForm.prefixes.some(function (prefix) {
            if (field.indexOf(prefix) == 0) {
                newLabel = field.substr(prefix.length);
                return true;
            }
            return false;
        });
        return newLabel;
    }
    
    function removePrefixes(ctx) {
        $('nobr').each(function(index, item) {
            if (hippocratic.fields.authorizationForm.ignoredLabels.indexOf(item.innerText) == -1)
                item.innerText = getFieldProperName(item.innerText);
        });
    }
    
    /**
    * Pre-rendering function implementation
    *
    * @method preRender
    * @param {Object} CSR context.
    */
    function preRenderCallback(ctx) {
        if (!hippocratic.authorizationFormRenderer.formDisabled) {
            hippocratic.authorizationFormRenderer.formDisabled = true;
            hippocratic.authorizationFormRenderer.practice = ctx.ListData.Items[0].PracticeName;
            hippocratic.utils.hideContent();
        }
        hippocratic.utils.separateGroups(ctx, hippocratic.fields.authorizationForm);
        hippocratic.authorizationFormRenderer.formContext = window["WPQ2FormCtx"];
        hippocratic.authorizationFormRenderer.processingStatus = hippocratic.authorizationFormRenderer.formContext.ListData.ProcessingStatus;
        hippocratic.authorizationFormRenderer.itemId = ctx.FormContext.itemAttributes.Id;
    }

    function preCacheNotificationTargetEmails() {
        EmailSender.getUserGroupEmails(hippocratic.authorizationFormRenderer.notificationTargetGroup);
    }
    
    function initializeForm() {
        ExecuteOrDelayUntilScriptLoaded(function () {       
            SPClientTemplates.TemplateManager.RegisterTemplateOverrides({
                Templates: {
                    Fields: hippocratic.utils.buildOverridenFieldsHash(hippocratic.fields.authorizationForm)
                },
                OnPreRender: preRenderCallback,
                OnPostRender: 
                [
                    hippocratic.security.updateFormForUserRole,
                    hippocratic.utils.updateCreatedBy,
                    renderFieldLabels
                ]
            });
            preCacheNotificationTargetEmails(); 
        }, 'sp.js');
    }

    initializeForm();
    return hippocratic;
}(hippocratic || {}));