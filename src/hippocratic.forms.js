var hippocratic = (function(hippocratic){

    hippocratic.forms = hippocratic.forms || {};

    hippocratic.forms.submitHandler = function() {
        return submitNewAndRedirectTo((new SP.ClientContext).get_url());
    };

    hippocratic.forms.submitAndNewHandler =  function () {
        return submitNewAndRedirectTo(window.location.href.replace('EditForm', 'NewForm'));
    }

    hippocratic.forms.editHandler = function () {
        STSNavigate(location.href.replace('DispForm', 'EditForm'));
    }

    
    hippocratic.forms.startProcessingHandler = function(){
        hippocratic.utils.updateProcessingStatus(hippocratic.authorizationFormRenderer.processingStatuses.Processing);
    }
    
    function getTodayDate(){
	    var tdate = new Date();
        var dd = tdate.getDate(); //yields day
        var MM = tdate.getMonth(); //yields month
        var yyyy = tdate.getFullYear(); //yields year
		var xxx = ( MM+1)+ "/" +dd + "/" + yyyy;
		return xxx;
    }

    hippocratic.forms.completeHandler = function() {
        if (!PreSaveItem()) return false;
		$("input[title='ProcessingStatus']").val(hippocratic.authorizationFormRenderer.processingStatuses.Completed);
		$("input[title='Completed By']").val(hippocratic.authorizationFormRenderer.currentUser);
		$("input[title='Completed Date']").val(getTodayDate());
        SPClientForms.ClientFormManager.SubmitClientForm('WPQ2');
    }
    
    hippocratic.forms.cancelHandler = function (redirectUrl) {
        var src = hippocratic.utils.getParameterByName("Src");
        STSNavigate(redirectUrl == null ? src : redirectUrl);
        return false;
    }

    function isNewForm() {
        // 3 stands for 'New form' control mode.
        return hippocratic.currentRenderer.formContext.FormControlMode == 3
    }
    
    function noValidationErrors() {
        return $('.ms-formvalidation:visible').length == 0;
    }
    
    function notificationNeeded() {
        return isNewForm() && noValidationErrors() && hippocratic.currentRenderer != hippocratic.insuranceFormRenderer;
    }


    
    /**
    * Sends the required notification to the target user group.
    *
    * @method getUserGroupEmails
    * @param {Function} Email sending callback.
    */   
    function sendNotification() {
        EmailSender.getUserGroupEmails(hippocratic.currentRenderer.notificationTargetGroup, function(emails) {
            var emailTemplate = EmailSender[hippocratic.currentRenderer.notificationTemplate](
                emails, 
                {
                    link: 'https://hippocraticsolutions.sharepoint.com' + (new SP.ClientContext).get_url() + '/Lists/' + hippocratic.utils.getListName() + '/AllItems.aspx',
                    practice: hippocratic.currentRenderer.practice
                }
            );
    
            EmailSender.sendEmailFromTemplate(emailTemplate);
        });
    }
    
    function delayedSendNotificationIfNeeded() {
        setTimeout(function() {
            if (notificationNeeded()) {
                sendNotification();
            }
        });
    }
    
    function submitNewAndRedirectTo(address) {
        if (!PreSaveItem()) return false;
        hippocratic.currentRenderer.formContext.RedirectInfo.redirectUrl = address;
        SPClientForms.ClientFormManager.SubmitClientForm('WPQ2');
        delayedSendNotificationIfNeeded();
    }

    function initializeForm(){
        $(function(){
            var url = window.location.href;
            var sourceArg = /[?&]Source=/;
            var match = url.match(sourceArg);
            if (match && match.length) {
                var source = hippocratic.utils.getParameterByName('Source');
                window.location.href = url.substring(0, match.index) + match[0][0] + 'Src=' + encodeURIComponent(source) 
            } else {
                $('#formContainer').show();
            }
        });
    }
    
    initializeForm();

    return hippocratic;
}(hippocratic || {}));