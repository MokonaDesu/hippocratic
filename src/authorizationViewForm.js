/**
* AuthorizationFormRenderer
* Used to render the Authorization input form
* based on current user group membership.
*/
var ViewAuthorizationFormRenderer = function () {
    /** 
    * Contains information about the fields on the
    * input form and their default renderers.
    *
    * @property fieldsInfo
    * @type {Object}
    */
    var fieldsData = {
        lastInGroup: [
            'IndividualNpi',
            'HippocraticLocation',
            'Dx',
            'EndDate',
            'SecondEndDate' ],
            
        prefixes: ['2nd ', '2ND ', 'Hippocratic', 'Hippocratic ', '1ST'],
        ignoredFields: ['SecondInsuranceProcedureCovered', 'SecondInsurance', 'FirstInsurance']
    };

    /**
    * Indicates whether the form is disabled for the
    * loading to complete.
    *
    * @property formDisabled
    * @type {Boolean}
    */
    var formDisabled = false;
    
    /**
    * Indicates whether the form has issued
    * the query to check for user permissions.
    * @property userGroupUpdateCalled
    * @type {Boolean}
    */
    var userGroupUpdateCalled = false;
    
    /**
    * When the suer permissions checking query has finished,
    * this field contains information about the internal user
    * group membership for current user.
    * @property userIsInternal
    * @type {Boolean} - undefined means query is not completed yet.
    */
    var userIsInternal;

    /**
    * Privileged user group name.
    *
    * @property internalUsersGroupName
    * @type {String}
    */
    var internalUsersGroupName = 'Internal Users';

    var extraButtonsAdded = false;
    
    /**
    * Contains default SharePoint field renderers
    *
    * @type {Array}
    */
    var defaultTemplates = SPClientTemplates._defaultTemplates.Fields.default.all.all;

    /**
    * Asynchronously checks if user is member of the
    * specified SP user group.
    *
    * @method isCurrentUserInGroup
    * @param {String} group name to check for
    * @param {Function} completion callback
    */
    function isCurrentUserInGroup(groupName, onComplete) {
        ExecuteOrDelayUntilScriptLoaded(function () {
            var currentContext = new SP.ClientContext.get_current();
            var currentWeb = currentContext.get_web();
            var currentUser = currentContext.get_web().get_currentUser();
            currentContext.load(currentUser);
            var allGroups = currentWeb.get_siteGroups();
            currentContext.load(allGroups);
            var group = allGroups.getByName(groupName);
            currentContext.load(group);
            var groupUsers = group.get_users();
            currentContext.load(groupUsers);

            currentContext.executeQueryAsync(onSuccess, onFailure);
            
            function onSuccess(sender, args) {
                var userInGroup = false;
                var groupUserEnumerator = groupUsers.getEnumerator();
                while (groupUserEnumerator.moveNext()) {
                    var groupUser = groupUserEnumerator.get_current();
                    if (groupUser.get_id() == currentUser.get_id()) {
                        userInGroup = true;
                        break;
                    }
                }
                onComplete(userInGroup, currentUser);
            }

            function onFailure(sender, args) {
                onComplete(false);
            }                 
        }, 'sp.js');
    }

    /**
    * Runs the query to find out if current
    * user is a member of internal user group;
    *
    * @method updateUserGroup
    */
    function updateUserGroup(callback) {  
        isCurrentUserInGroup(internalUsersGroupName, function(isMember, currentUser) {
            userIsInternal = isMember;
            callback(currentUser);
        });
    }
    
    /**
    * Performs the security checks and updates 
    * the input form controls according to 
    * current user's user group.
    *
    * @method requestUserPermissionsAndUpdateForm
    */
    function requestUserPermissionsAndUpdateForm(ctx) {
        if (!userGroupUpdateCalled) {
            userGroupUpdateCalled = true;
            updateUserGroup(function(currentUser) {
                showContent();
            });
        }
    }
    
    /**
    * Adds extra table rows between the input
    * control groups on pre-render stage.
    *
    * @method separateGroups
    * @param {Object} CSR context.
    */
    function separateGroups(ctx) {
        ctx.ListSchema.Field.forEach(function(field) {
            if (fieldsData.lastInGroup.indexOf(field.Name) != -1) {
                var span = $get(ctx.FormUniqueId + ctx.FormContext.listAttributes.Id + field.Name);
                var tr = document.createElement('tr');
                tr.style.height = '50px';
                tr.innerHTML='<td colspan="2">&nbsp;</td>';
                var fieldTr = span.parentNode.parentNode;
                fieldTr.parentNode.insertBefore(tr, fieldTr.nextSibling);
            }
        });
    }

    /**
    * Shows the input form after the security checks
    * are complete
    *
    * @method showForm
    */
    function showContent() {
        addExtraButtons();
        $('#contentBox').fadeIn();
    }
    
    /**
    * Hides the input form until the required
    * security checks are done.
    *
    * @method disableFormUntilLoaded
    * @param {Object} CSR context.
    */
    function hideContent() {
        $('#contentBox').hide();
    }

    
    function getFieldProperName(field) {
        var newLabel = field;
        fieldsData.prefixes.some(function (prefix) {
            if (field.indexOf(prefix) == 0) {
                newLabel = field.substr(prefix.length);
                return true;
            }
            return false;
        });
        return newLabel;
    }
    
    /**
    * Changes out labels on certain fields.
    *
    * @method postRenderLocationLabel
    * @param {Object} CSR context.
    */
    function postRenderFieldLabels(ctx) {
        var fieldName = ctx.ListSchema.Field[0].Name;
        if (fieldsData.ignoredFields.indexOf(fieldName) != -1) return;
      
        var labelHeading = $('a[name="SPBookmark_' + fieldName + '"]').parent();
        var oldLabel = labelHeading.contents().last().remove().text();
        labelHeading.append(getFieldProperName(oldLabel));
    }
    
    /**
    * Pre-rendering function implementation
    *
    * @method preRender
    * @param {Object} CSR context.
    */
    function preRender(ctx) {
        if (!formDisabled) {
            formDisabled = true;
            hideContent();
        }
        separateGroups(ctx);
    }

    
    /**
    * Adds new button to the SP form toolbar. 
    *
    * @method pushNewButton
    * @param {String} Button outer html.
    */
    function pushNewButton(outerHtml) {
        $('table.ms-formtoolbar')
            .last()
            .find('.ms-separator')
            .first()
            .after('<td class="ms-toolbar">' + outerHtml + '</td><td class="ms-separator">&nbsp;</td>');
    }
    
    /**
    * Removes the default buttons from SP form toopbar.
    *
    * @method removeExistingButtons
    */    
    function removeExistingButtons() {
        $('table.ms-formtoolbar')
            .last()
            .find('tr')
            .addClass('custom-buttons-container')
            .empty()
            .append('<td class="ms-separator"></td>');
        document.getElementById('Ribbon.ListForm.Display.Manage.EditItem-Large').style.display = 'none';
    }
    
    function canEdit() {
        return true;
    }
    
    /**
    * Adds the required extra buttons.
    *
    * @method addExtraButtons
    */        
    function addExtraButtons() {
        removeExistingButtons();
      
        pushNewButton('<input type="button" name="cancel" value="Cancel" onclick="cancelHandler()">');
        
        if (canEdit()) pushNewButton('<input type="button" value="Edit" onclick="editHandler()">');
    }

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides({
        OnPreRender: [preRender],
        OnPostRender: [requestUserPermissionsAndUpdateForm, postRenderFieldLabels]
    });
}

ViewAuthorizationFormRenderer();

function editHandler() {
    JSRequest.EnsureSetup();
    STSNavigate((new SP.ClientContext).get_url() + '/Lists/AuthorizationFormsList/EditForm.aspx?ID=' + JSRequest.QueryString["ID"]);
    return false;
}

function cancelHandler() {
    STSNavigate((new SP.ClientContext).get_url());
    return false;
}

