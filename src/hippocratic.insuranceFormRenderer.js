/* global SPClientTemplates */
/**
* AuthorizationFormRenderer
*/
var hippocratic = (function (hippocratic) {
    hippocratic.insuranceFormRenderer = hippocratic.insuranceFormRenderer || {};
    hippocratic.currentRenderer = hippocratic.insuranceFormRenderer;
    hippocratic.insuranceFormRenderer.formContext = null;
    hippocratic.insuranceFormRenderer.itemId = -1;

    /**
    * A value indicating whether extra buttons section is added.
    * @property extraButtonsAdded
    * @type {String}
    */
    hippocratic.insuranceFormRenderer.extraButtonsAdded = false;
    
    hippocratic.insuranceFormRenderer.addCustomButtons = function(userIsInternal) {
        hippocratic.utils.removeExistingButtons();
        hippocratic.utils.createUpperToolbar();
        hippocratic.utils.addCancelButton();
        hippocratic.utils.addSubmitButtons();
    }

    /**
    * Retrieves the type of the field to swap label for
    * 
    * @method getFieldType
    */
    function getFieldType(fieldName) {
        if (fieldName.indexOf('CPTCode') == 0)
            return '_$TextField';
        else
            return '_$CurrencyField';
    }    
    
    /**
    * Changes out labels on certain fields.
    *
    * @method postRenderLocationLabel
    * @param {Object} CSR context.
    */
    function renderFieldLabels(ctx) {
        var fieldName = ctx.ListSchema.Field[0].Name;
        
        if (fieldName == 'CreatedBy') {
            $(document.getElementById('CreatedBy_' + ctx.ListSchema.Field[0].Id + '_$TextField'))
                .closest('tr')
                .find('nobr')
                .text('Created By');
            return;
        }
        
        var changeTo;
      
        var postfixTrimNeeded = hippocratic.fields.insuranceForm.removePostfixIn.some(function(field) {
            var needed = fieldName.indexOf(field) == 0;
            if (needed) changeTo = field;
            return needed;
        });
      
        if (postfixTrimNeeded) {
            var containingTr;
            if (fieldName.indexOf('HippocraticStatus') == 0) {
                changeTo = 'Status';
                containingTr = $(document.getElementById(fieldName + '_' + ctx.ListSchema.Field[0].Id + '_MultiChoiceTable')).closest('tr');
            } else {
                if (fieldName.indexOf('CPTCode') == 0) changeTo = 'CPT Code';
                containingTr = $(document.getElementById(fieldName + '_' + ctx.ListSchema.Field[0].Id + getFieldType(fieldName))).closest('tr');
            }
            containingTr.find('nobr').text(changeTo);            
        }      
    }
    
    /**
    * Pre-rendering function implementation
    *
    * @method preRender
    * @param {Object} CSR context.
    */
    function preRenderCallback(ctx) {
        if (!hippocratic.insuranceFormRenderer.formDisabled) {
            hippocratic.insuranceFormRenderer.formDisabled = true;
            hippocratic.utils.hideContent();
        }
        hippocratic.utils.separateGroups(ctx, hippocratic.fields.insuranceForm);
        hippocratic.insuranceFormRenderer.formContext = window["WPQ2FormCtx"];
        hippocratic.insuranceFormRenderer.processingStatus = hippocratic.insuranceFormRenderer.formContext.ListData.ProcessingStatus;
        hippocratic.insuranceFormRenderer.itemId = ctx.FormContext.itemAttributes.Id;
    }

    function initializeForm(){
        SPClientTemplates.TemplateManager.RegisterTemplateOverrides({
                Templates: {
                    Fields: hippocratic.utils.buildOverridenFieldsHash(hippocratic.fields.paymentsForm)
                },
                OnPreRender: preRenderCallback,
                OnPostRender: 
                [
                    hippocratic.utils.updateCreatedBy,
                    renderFieldLabels
                ]
            });
    
        ExecuteOrDelayUntilScriptLoaded(function () {        
            hippocratic.security.updateFormForUserRole();
        }, 'sp.js');
    }

    initializeForm();
    return hippocratic;
}(hippocratic || {}));