/* global SPClientTemplates */
/**
* AuthorizationFormRenderer
*/
var hippocratic = (function (hippocratic) {
    hippocratic.paymentsFormRenderer = hippocratic.paymentsFormRenderer || {};
    hippocratic.currentRenderer = hippocratic.paymentsFormRenderer;
    hippocratic.paymentsFormRenderer.formContext = null;
    hippocratic.paymentsFormRenderer.itemId = -1;
    hippocratic.paymentsFormRenderer.practice;
    
    hippocratic.paymentsFormRenderer.notificationTargetGroup = 
        hippocratic.utils.getListName() == 'PatientPayments'
            ? 'Patients Form Watchers'
            : 'Copay Form Watchers';
        
    hippocratic.paymentsFormRenderer.notificationTemplate =
        hippocratic.utils.getListName() == 'PatientPayments'
            ? 'newPatientPaymentFormTemplate'
            : 'newCopayPaymentFormTemplate';    
    
    /**
    * A value indicating whether extra buttons section is added.
    * @property extraButtonsAdded
    * @type {String}
    */
    hippocratic.paymentsFormRenderer.extraButtonsAdded = false;
    
    hippocratic.paymentsFormRenderer.addCustomButtons = function(userIsInternal) {
        hippocratic.utils.removeExistingButtons();
        hippocratic.utils.createUpperToolbar();
        hippocratic.utils.addCancelButton();
        
        if (!hippocratic.security.userIsInternal) {
            hippocratic.utils.addSubmitButtons();
        }
    }
    
    /**
    * Changes out labels on certain fields.
    *
    * @method postRenderLocationLabel
    * @param {Object} CSR context.
    */
    function renderFieldLabels(ctx) {
      switch(ctx.ListSchema.Field[0].Name) {  
        case 'CreatedBy': 
          var containingTr = $(document.getElementById('CreatedBy_' + ctx.ListSchema.Field[0].Id + '_$TextField')).closest('tr');
          var nobt = containingTr.find('nobr').text(containingTr.find('nobr').text().replace('Hippocratic Created By', 'Created By'));
          break;
      }
    }
    
    /**
    * Pre-rendering function implementation
    *
    * @method preRender
    * @param {Object} CSR context.
    */
    function preRenderCallback(ctx) {
        if (!hippocratic.paymentsFormRenderer.formDisabled) {
            hippocratic.paymentsFormRenderer.formDisabled = true;
            hippocratic.paymentsFormRenderer.practice = ctx.ListData.Items[0].PracticeName;
            hippocratic.utils.hideContent();
        }
        hippocratic.utils.separateGroups(ctx, hippocratic.fields.paymentsForm);
        hippocratic.paymentsFormRenderer.formContext = window["WPQ2FormCtx"];
        hippocratic.paymentsFormRenderer.processingStatus = hippocratic.paymentsFormRenderer.formContext.ListData.ProcessingStatus;
        hippocratic.paymentsFormRenderer.itemId = ctx.FormContext.itemAttributes.Id;
    }

    function preCacheNotificationTargetEmails() {
        EmailSender.getUserGroupEmails(hippocratic.paymentsFormRenderer.notificationTargetGroup);
    }
    
    function initializeForm(){
        ExecuteOrDelayUntilScriptLoaded(function () {        
            SPClientTemplates.TemplateManager.RegisterTemplateOverrides({
                Templates: {
                    Fields: hippocratic.utils.buildOverridenFieldsHash(hippocratic.fields.paymentsForm)
                },
                OnPreRender: preRenderCallback,
                OnPostRender: 
                [
                    hippocratic.security.updateFormForUserRole,
                    hippocratic.utils.updateCreatedBy,
                    renderFieldLabels
                ]
            });
            preCacheNotificationTargetEmails(); 
        }, 'sp.js');
    }

    initializeForm();
    return hippocratic;
}(hippocratic || {}));