var hippocratic = (function(hippocratic){
    hippocratic.fields = hippocratic.fields || {};
    
    hippocratic.fields.authorizationForm = {
        readOnlyFields: [
            'PracticeName',
            'TaxId',
            'GroupNpi',
            'IndividualNpi',
            'CreatedDate',
            'CreatedBy',
            'CompletedDate',
            'CompletedBy'],

        protectedFields: [
            'FirstInsuranceProcedureCovered',
            'Deductible',
            'Amount',
            'Remaining',
            'Coinsurance',
            'Percentage',
            'Authorization',
            'AuthorizationNo',
            'StartDate1',
            'EndDate1',
            'SecondInsuranceProcedureCovered',
            'SecondDeductible',
            'SecondAmount',
            'SecondRemaining',
            'SecondCoinsurance',
            'SecondPercentage',
            'SecondAuthorization',
            'SecondAuthorizationNo',
            'SecondStartDate',
            'SecondEndDate',
            'Representative',
            'ReferenceNo',
            'Notes1' ],

        lastInGroup: [
            'IndividualNpi',
            'HippocraticLocation',
            'Dx',
            'EndDate',
            'SecondEndDate' ],
            
        hiddenFields: [
            'ProcessingStatus'
        ],
        prefixes: ['2nd ', '2ND ', 'Hippocratic', 'Hippocratic ', '1ST'],
        ignoredFields: ['SecondInsuranceProcedureCovered', 'SecondInsurance', 'FirstInsurance'],
        ignoredLabels: ['2nd Insurance Procedure Covered', '2ND INSURANCE', '1ST INSURANCE *']
    };
    
    hippocratic.fields.paymentsForm = {
        readOnlyFields: ['CreatedBy', 'CreatedDate'],
        protectedFields: [],
        hiddenFields: [],
        lastInGroup: []
    };
    
    hippocratic.fields.insuranceForm = {
        readOnlyFields: ['CreatedBy', 'CreatedDate'],
        protectedFields: [],
        hiddenFields: [],
        lastInGroup: ['DateOfService', 'HippocraticStatus', 'HippocraticStatus_1', 'HippocraticStatus_2', 'HippocraticStatus_3',
            'HippocraticStatus_4', 'HippocraticStatus_5', 'HippocraticStatus_6', 'HippocraticStatus_7', 'HippocraticStatus_8',
            'HippocraticStatus_9'],
        removePostfixIn: ['CPTCode', 'Allowed', 'Deductible', 'Coinsurance', 'Copay', 'Paid', 'HippocraticStatus']
    }

    return hippocratic;
}(hippocratic || {}));