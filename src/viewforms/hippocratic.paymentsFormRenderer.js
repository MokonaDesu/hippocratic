/* global SPClientTemplates */
/**
* AuthorizationFormRenderer
*/
var hippocratic = (function (hippocratic) {
    hippocratic.paymentsFormRenderer = hippocratic.paymentsFormRenderer || {};
    hippocratic.currentRenderer = hippocratic.paymentsFormRenderer;
    hippocratic.paymentsFormRenderer.formContext = null;
    hippocratic.paymentsFormRenderer.itemId = -1;

    /**
    * A value indicating whether extra buttons section is added.
    * @property extraButtonsAdded
    * @type {String}
    */
    hippocratic.paymentsFormRenderer.extraButtonsAdded = false;
    
    hippocratic.paymentsFormRenderer.addCustomButtons = function(userIsInternal) {
        hippocratic.utils.removeExistingButtons();
        hippocratic.utils.createUpperToolbar();
        hippocratic.utils.addCancelButton();
        hippocratic.utils.addEditButton();
    }

    /**
    * Changes out labels on certain fields.
    *
    * @method postRenderLocationLabel
    * @param {Object} CSR context.
    */
    function renderFieldLabels(ctx) {
        var fieldName = ctx.ListSchema.Field[0].Name;      
        var labelHeading = $('a[name="SPBookmark_' + fieldName + '"]').parent();
        var oldLabel = labelHeading.contents().last().remove().text();
        if (oldLabel.indexOf('Hippocratic ') == 0) {
            labelHeading.append(oldLabel.replace('Hippocratic ', ''));
        } else {
            labelHeading.append(oldLabel);
        }
    }
    
    /**
    * Pre-rendering function implementation
    *
    * @method preRender
    * @param {Object} CSR context.
    */
    function preRenderCallback(ctx) {
        if (!hippocratic.paymentsFormRenderer.formDisabled) {
            hippocratic.paymentsFormRenderer.formDisabled = true;
            hippocratic.utils.hideContent();
        }
        hippocratic.utils.separateGroups(ctx, hippocratic.fields.authorizationForm);
        hippocratic.paymentsFormRenderer.formContext = window["WPQ2FormCtx"];
        hippocratic.paymentsFormRenderer.processingStatus = hippocratic.paymentsFormRenderer.formContext.ListData.ProcessingStatus;
        hippocratic.paymentsFormRenderer.itemId = ctx.FormContext.itemAttributes.Id;
    }

    function initializeForm(){
        SPClientTemplates.TemplateManager.RegisterTemplateOverrides({
                OnPreRender: preRenderCallback,
                OnPostRender: 
                [
                    renderFieldLabels
                ]
            });
    
        ExecuteOrDelayUntilScriptLoaded(function () {        
            hippocratic.security.updateFormForUserRole();
        }, 'sp.js');
    }

    initializeForm();
    return hippocratic;
}(hippocratic || {}));