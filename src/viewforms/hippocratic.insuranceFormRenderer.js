/* global SPClientTemplates */
/**
* InsuranceFormRenderer
*/
var hippocratic = (function (hippocratic) {
    hippocratic.insuranceFormRenderer = hippocratic.insuranceFormRenderer || {};
    hippocratic.currentRenderer = hippocratic.insuranceFormRenderer;
    hippocratic.insuranceFormRenderer.formContext = null;
    hippocratic.insuranceFormRenderer.itemId = -1;

    /**
    * A value indicating whether extra buttons section is added.
    * @property extraButtonsAdded
    * @type {String}
    */
    hippocratic.insuranceFormRenderer.extraButtonsAdded = false;
    
    hippocratic.insuranceFormRenderer.addCustomButtons = function(userIsInternal) {
        hippocratic.utils.removeExistingButtons();
        hippocratic.utils.createUpperToolbar();
        hippocratic.utils.addCancelButton();
        hippocratic.utils.addEditButton();
    }

    /**
    * Retrieves the type of the field to swap label for
    * 
    * @method getFieldType
    */
    function getFieldType(fieldName) {
        if (fieldName.indexOf('CPTCode') == 0)
            return '_$TextField';
        else
            return '_$CurrencyField';
    }    
    
    function getFieldProperName(field) {
        var postfix = /_\d+/;
        var match = field.match(postfix);
        if (match && match.length) {
            return field.substr(0, match.index);
        }
        return field;
    }
    
    /**
    * Changes out labels on certain fields.
    *
    * @method postRenderLocationLabel
    * @param {Object} CSR context.
    */
    function renderFieldLabels(ctx) {
        var fieldName = ctx.ListSchema.Field[0].Name;      
        var labelHeading = $('a[name="SPBookmark_' + fieldName + '"]').parent();
        var oldLabel = labelHeading.contents().last().remove().text();
        if (oldLabel.indexOf('HippocraticStatus') == 0) {
            labelHeading.append('Status');
        } else if (fieldName == 'CreatedBy') {
            labelHeading.append('Created By');
        } else {
            labelHeading.append(getFieldProperName(oldLabel));
        }
    }
    
    /**
    * Pre-rendering function implementation
    *
    * @method preRender
    * @param {Object} CSR context.
    */
    function preRenderCallback(ctx) {
        if (!hippocratic.insuranceFormRenderer.formDisabled) {
            hippocratic.insuranceFormRenderer.formDisabled = true;
            hippocratic.utils.hideContent();
        }
        hippocratic.utils.separateGroups(ctx, hippocratic.fields.insuranceForm);
        hippocratic.insuranceFormRenderer.formContext = window["WPQ2FormCtx"];
        hippocratic.insuranceFormRenderer.processingStatus = hippocratic.insuranceFormRenderer.formContext.ListData.ProcessingStatus;
        hippocratic.insuranceFormRenderer.itemId = ctx.FormContext.itemAttributes.Id;
    }

    function initializeForm(){
        SPClientTemplates.TemplateManager.RegisterTemplateOverrides({
                OnPreRender: preRenderCallback,
                OnPostRender: 
                [
                    renderFieldLabels
                ]
            });
    
        ExecuteOrDelayUntilScriptLoaded(function () {        
            hippocratic.security.updateFormForUserRole();
        }, 'sp.js');
    }

    initializeForm();
    return hippocratic;
}(hippocratic || {}));