/* global SPClientTemplates */
/**
* AuthorizationFormRenderer
*/
var hippocratic = (function (hippocratic) {
    hippocratic.authorizationFormRenderer = hippocratic.authorizationFormRenderer || {};
    hippocratic.currentRenderer = hippocratic.authorizationFormRenderer;
    hippocratic.authorizationFormRenderer.formContext = null;
    hippocratic.authorizationFormRenderer.itemId = -1;

    /**
    * A value indicating whether extra buttons section is added.
    * @property extraButtonsAdded
    * @type {String}
    */
    hippocratic.authorizationFormRenderer.extraButtonsAdded = false;
    
    hippocratic.authorizationFormRenderer.addCustomButtons = function(userIsInternal) {
        hippocratic.utils.removeExistingButtons();
        hippocratic.utils.createUpperToolbar();
        hippocratic.utils.addCancelButton();
        hippocratic.utils.addEditButton();
    }

    function getFieldProperName(field) {
        var newLabel = field;
        hippocratic.fields.authorizationForm.prefixes.some(function (prefix) {
            if (field.indexOf(prefix) == 0) {
                newLabel = field.substr(prefix.length);
                return true;
            }
            return false;
        });
        return newLabel;
    }
    
    /**
    * Changes out labels on certain fields.
    *
    * @method postRenderLocationLabel
    * @param {Object} CSR context.
    */
    function renderFieldLabels(ctx) {
        var fieldName = ctx.ListSchema.Field[0].Name;
        if (hippocratic.fields.authorizationForm.ignoredFields.indexOf(fieldName) != -1) return;
      
        var labelHeading = $('a[name="SPBookmark_' + fieldName + '"]').parent();
        var oldLabel = labelHeading.contents().last().remove().text();
        
        if (oldLabel == 'ProcessingStatus') 
            labelHeading.closest('tr').css('display', 'none');
        else
            labelHeading.append(getFieldProperName(oldLabel));
    }
    
    /**
    * Pre-rendering function implementation
    *
    * @method preRender
    * @param {Object} CSR context.
    */
    function preRenderCallback(ctx) {
        if (!hippocratic.authorizationFormRenderer.formDisabled) {
            hippocratic.authorizationFormRenderer.formDisabled = true;
            hippocratic.utils.hideContent();
        }
        hippocratic.utils.separateGroups(ctx, hippocratic.fields.authorizationForm);
        hippocratic.authorizationFormRenderer.formContext = window["WPQ2FormCtx"];
        hippocratic.authorizationFormRenderer.processingStatus = hippocratic.authorizationFormRenderer.formContext.ListData.ProcessingStatus;
        hippocratic.authorizationFormRenderer.itemId = ctx.FormContext.itemAttributes.Id;
    }

    function initializeForm(){
        SPClientTemplates.TemplateManager.RegisterTemplateOverrides({
                OnPreRender: preRenderCallback,
                OnPostRender: 
                [
                    renderFieldLabels
                ]
            });
    
        ExecuteOrDelayUntilScriptLoaded(function () {        
            hippocratic.security.updateFormForUserRole();
        }, 'sp.js');
    }

    initializeForm();
    return hippocratic;
}(hippocratic || {}));